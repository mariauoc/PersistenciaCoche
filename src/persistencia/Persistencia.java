/*
 * Ejemplo de persistencia en XML
 */
package persistencia;

/**
 *
 * @author mfontana
 */
public class Persistencia {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Fichero miFichero = new Fichero("coche.xml");
        Coche miCoche;
        // Leo del fichero
        miCoche = (Coche) miFichero.leer();
        if (miCoche == null) {
            miCoche = new Coche("1234ABC", "Azul", 8);
            miFichero.grabar(miCoche);
            System.out.println("Datos guardados");
        } else {
            System.out.println("He leído del fichero");
        }
        System.out.println(miCoche);
    }

}
