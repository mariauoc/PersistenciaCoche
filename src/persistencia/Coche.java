/*
 * Entidad Coche - JavaBean
 */
package persistencia;

import java.io.Serializable;

/**
 *
 * @author mfontana
 */
public class Coche implements Serializable {

    private String matricula;
    private String color;
    private int puertas;

    public Coche() {
        this("", "", 0);
    }

    public Coche(String matricula, String color, int puertas) {
        this.matricula = matricula;
        this.color = color;
        this.puertas = puertas;
    }

    public int getPuertas() {
        return puertas;
    }

    public void setPuertas(int puertas) {
        this.puertas = puertas;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    @Override
    public String toString() {
        return "Coche{" + "matricula=" + matricula + ", color=" + color + ", puertas=" + puertas + '}';
    }

    
    
}
